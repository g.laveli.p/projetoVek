-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dbvek
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema dbvek
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbvek` DEFAULT CHARACTER SET utf8 ;
USE `dbvek` ;

-- -----------------------------------------------------
-- Table `dbvek`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbvek`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `cpf` VARCHAR(20) NOT NULL,
  `telefone` VARCHAR(15) NOT NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbvek`.`operadora`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dbvek`.`operadora` (
  `id_cartao` INT NOT NULL AUTO_INCREMENT,
  `concorrente` VARCHAR(45) NOT NULL,
  `ramo` VARCHAR(45) NOT NULL,
  `taxa_debto_concorrente` DECIMAL(5,2) NOT NULL,
  `desconto_debito` DECIMAL(5,2) NOT NULL,
  `taxa_credito_concorrente` DECIMAL(5,2) NOT NULL,
  `desconto_credito` DECIMAL(5,2) NOT NULL,
  `aceite` INT NULL,
  `usuario_id_usuario` INT NOT NULL,
  PRIMARY KEY (`id_cartao`),
  INDEX `fk_operadora_usuario_idx` (`usuario_id_usuario` ASC) VISIBLE,
  CONSTRAINT `fk_operadora_usuario`
    FOREIGN KEY (`usuario_id_usuario`)
    REFERENCES `dbvek`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
