import java.sql.SQLException;
import java.util.ArrayList;

import br.com.vek.dao.UserDAO;
import br.com.vek.domain.User;

public class Program {

	public static void main(String[] args) {

		/*
		 * UserDAO udao = new UserDAO();
		 * 
		 * User usuario1 = new User(); usuario1.setCpf("07773002908");
		 * usuario1.setTelefone("(48)32062553");
		 * usuario1.setEmail("Keren.anne@gmail.com");
		 * 
		 * try { udao.salvar(usuario1); System.out.println("Sucesso ao salvar Usuario");
		 * 
		 * } catch (SQLException e) { // TODO Auto-generated catch block
		 * System.out.println("Erro ao salvar"); e.printStackTrace(); }
		 */

		/*
		 * UserDAO udao2 = new UserDAO();
		 * 
		 * User deletarUser = new User();
		 * 
		 * deletarUser.setId(3);
		 * 
		 * try { udao2.deleteUser(deletarUser); System.out.println("Usuario "); } catch
		 * (SQLException e) {
		 * System.out.println("Removido! so que nao... checa os log ai doido.");
		 * e.printStackTrace(); }
		 */

		/*
		 * User u1 = new User(); u1.setId(3L);
		 * 
		 * UserDAO uDao = new UserDAO();
		 * 
		 * try { User u3 = uDao.buscarPorId(u1);
		 * 
		 * System.out.println(u3); u3.toString(); } catch (SQLException e) {
		 * e.printStackTrace(); }
		 */

		
		  UserDAO uDao = new UserDAO(); try { ArrayList<User> listaUser =
		  uDao.buscarTodos(); for (User u : listaUser) { System.out.println(u + "\n");
		  }
		  
		  } catch (SQLException e) { System.out.println("Errou!"); e.printStackTrace();
		  }
		 

		/*
		 * User usuario = new User(); usuario.setCpf("0");
		 * 
		 * UserDAO udao = new UserDAO();
		 * 
		 * try { ArrayList<User> lista = udao.buscarPorCpf(usuario); for (User u :
		 * lista) { System.out.println(u); } } catch (SQLException e) {
		 * e.printStackTrace(); }
		 */
	}

}
