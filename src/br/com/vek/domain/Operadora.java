package br.com.vek.domain;

public class Operadora {

	private long id;
	private String concorrente;
	private String ramo;
	private Double taxaDebitoConcorrente;
	private Double descontoDebito;
	private Double taxaCreditoConcorrente;
	private Double descontoCredito;
	private long aceite;
	private User user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getConcorrente() {
		return concorrente;
	}

	public void setConcorrente(String concorrente) {
		this.concorrente = concorrente;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public Double getTaxaDebitoConcorrente() {
		return taxaDebitoConcorrente;
	}

	public void setTaxaDebitoConcorrente(Double taxaDebitoConcorrente) {
		this.taxaDebitoConcorrente = taxaDebitoConcorrente;
	}

	public Double getDescontoDebito() {
		return descontoDebito;
	}

	public void setDescontoDebito(Double descontoDebito) {
		this.descontoDebito = descontoDebito;
	}

	public Double getTaxaCreditoConcorrente() {
		return taxaCreditoConcorrente;
	}

	public void setTaxaCreditoConcorrente(Double taxaCreditoConcorrente) {
		this.taxaCreditoConcorrente = taxaCreditoConcorrente;
	}

	public Double getDescontoCredito() {
		return descontoCredito;
	}

	public void setDescontoCredito(Double descontoCredito) {
		this.descontoCredito = descontoCredito;
	}

	public long getAceite() {
		return aceite;
	}

	public void setAceite(long aceite) {
		this.aceite = aceite;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
