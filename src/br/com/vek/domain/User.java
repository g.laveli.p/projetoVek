package br.com.vek.domain;

public class User {

	private long id;
	private String cpf;
	private String telefone;
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder saida = new StringBuilder();
		saida.append("Id: " + id + " CPF: " + cpf + " Telefone: " + telefone + " E-mai: " + email);
		return saida.toString();
	}

}