package br.com.vek.bean;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;

import br.com.vek.dao.UserDAO;
import br.com.vek.domain.User;

@ManagedBean(name = "MBUsuarios")
@ViewScoped
public class UserBean {

	private ListDataModel<User> itens;

	public ListDataModel<User> getItens() {
		return itens;
	}

	public void setItens(ListDataModel<User> itens) {
		this.itens = itens;
	}

	@PostConstruct
	public void prepararPesquisa() {
		try {
			UserDAO dao = new UserDAO();
			ArrayList<User> lista = dao.buscarTodos();
			itens = new ListDataModel<User>(lista);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
