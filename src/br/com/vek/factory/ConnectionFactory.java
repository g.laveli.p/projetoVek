package br.com.vek.factory;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {

	private static Connection conn = null;

	public static Connection getConnection() {
		if (conn == null) {

			try {
				// Em caso de erro de registro de driver, remova o comentario do c�digo abaixo!
				DriverManager.registerDriver(new com.mysql.jdbc.Driver());
				Properties props = loadProperties();
				String url = props.getProperty("dburl");
				conn = DriverManager.getConnection(url, props);
				System.out.println("Conectado!");
			} catch (SQLException e) {
				throw new DbException(e.getMessage());
			}

		}
		return conn;
	}

	public static void closeConnectiom() {
		if (conn != null) {
			try {
				conn.close();
				System.out.println("Conex�o encerrada!");
			} catch (SQLException e) {
				throw new DbException(e.getMessage());
			}
		}
	}

	private static Properties loadProperties() {

		try (FileInputStream fis = new FileInputStream("db.properties")) {
			Properties props = new Properties();
			props.load(fis);
			return props;
		} catch (IOException e) {
			throw new DbException(e.getMessage());
		}
	}

}
