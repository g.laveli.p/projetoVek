package br.com.vek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.vek.domain.User;
import br.com.vek.factory.ConnectionFactory;

public class UserDAO {

	Connection conn = ConnectionFactory.getConnection();

	public void salvar(User user) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("insert into usuario (cpf, telefone, email) values (?, ?, ?) ");

		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setString(1, user.getCpf());
		ps.setString(2, user.getTelefone());
		ps.setString(3, user.getEmail());

		ps.executeUpdate();

	}

	public void deletar(User user) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("delete from usuario where id_usuario = ?");

		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, user.getId());

		ps.executeUpdate();
	}

	public User buscarPorId(User user) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("select id_usuario, cpf, telefone, email from usuario where id_usuario = ?");

		Connection conn = ConnectionFactory.getConnection();

		PreparedStatement ps = conn.prepareStatement(sql.toString());

		ps.setLong(1, user.getId());

		ResultSet rs = ps.executeQuery();

		User userRetornado = null;

		if (rs.next()) {
			userRetornado = new User();

			userRetornado.setId(rs.getLong("id_usuario"));
			userRetornado.setCpf(rs.getString("cpf"));
			userRetornado.setTelefone(rs.getString("telefone"));
			userRetornado.setEmail(rs.getString("email"));
		}

		return userRetornado;
	}

	public ArrayList<User> buscarTodos() throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("select id_usuario, cpf, telefone, email ");
		sql.append("from usuario order by id_usuario asc");

		Connection conn = ConnectionFactory.getConnection();

		PreparedStatement ps = conn.prepareStatement(sql.toString());

		ResultSet rs = ps.executeQuery();

		ArrayList<User> listaUser = new ArrayList<>();

		while (rs.next()) {
			User u = new User();
			u.setId(rs.getLong("id_usuario"));
			u.setCpf(rs.getString("cpf"));
			u.setTelefone(rs.getString("telefone"));
			u.setEmail(rs.getString("email"));

			listaUser.add(u);
		}

		return listaUser;
	}

	public ArrayList<User> buscarPorCpf(User user) throws SQLException {

		StringBuilder sql = new StringBuilder();
		sql.append("select id_usuario, cpf, telefone, email ");
		sql.append("from usuario where cpf like ? ");
		sql.append("order by id_usuario asc");

		Connection conn = ConnectionFactory.getConnection();

		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setString(1, "%" + user.getCpf() + "%");

		ResultSet rs = ps.executeQuery();

		ArrayList<User> listaUser = new ArrayList<>();

		while (rs.next()) {
			User u = new User();
			u.setId(rs.getLong("id_usuario"));
			u.setCpf(rs.getString("cpf"));
			u.setTelefone(rs.getString("telefone"));
			u.setEmail(rs.getString("email"));

			listaUser.add(u);
		}

		return listaUser;
	}
}